import logo from './logo.svg';
import './App.css';

function App() {
  return (
    <div className="App">
      <header className="App-header">
        <img src={logo} className="App-logo" alt="logo" />
        <p>
          editar documento para subir na main <code>src/App.js</code> and save to reload.
        </p>
        <a
          className="App-link"
          href="https://reactjs.org"
          target="_blank"
          rel="noopener noreferrer"
        >
          Ola mundo Tudo bem com vc? mais uma vez aqui testando 
          como se faz o primeiro git
        </a>
      </header>
    </div>
  );
}

export default App;
